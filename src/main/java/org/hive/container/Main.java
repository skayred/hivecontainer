package org.hive.container;

import com.google.inject.Injector;
import org.hive.container.lib.ApplicationRegister;
import org.hive.container.lib.InjectorSingleton;
import org.hive.container.proxy.ContainerProxy;
import org.hive.container.lib.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.Endpoint;
import java.io.FileInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;


public class Main {

    private static Properties properties;

    public static int test(Integer ... numbers) {
        return numbers.length;
    }

    public static void main(String[] args) throws SAXException, ParserConfigurationException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, ClassNotFoundException {
        //  Если не указан джарник, то атата
        if (args.length != 2) {
            Main.printUsage();

            System.exit(1);
        }

        Injector injector = InjectorSingleton.getInjector();
        ApplicationRegister appRegister = injector.getInstance(ApplicationRegister.class);

        //  Берем местоположение джарника и конфига
        String jarLocation = args[0];
        String propertiesLocation = args[1];

        //  Вынимаем параметры приложения
        Main.loadAppProperties(propertiesLocation);

        String appName = appRegister.addApplication(jarLocation);

        //  If everything is ok
        Main.publishEndPoint(appName);

        //  А вот тут начинаются примеры
        //  Первый метод
//        String methodName = applicationMethods.get(0).getName();
//
//        System.out.println(methodName);
//
//        Method method = applicationMethods.get(0).getReflectionMethod(jcl, obj);
//        Object result = method.invoke(obj, 1, 2);
//
//        System.out.println(result);
//
//
//        //  Второй метод
//        methodName = applicationMethods.get(1).getName();
//
//        System.out.println(methodName);
//
//        method = applicationMethods.get(1).getReflectionMethod(jcl, obj);
//        result = method.invoke(obj, 1.0, 2.0);
//
//        System.out.println(result);
//
//
//        //  Третий метод
//        methodName = applicationMethods.get(2).getName();
//
//        System.out.println(methodName);
//
//        method = applicationMethods.get(2).getReflectionMethod(jcl, obj);
//
//        Object hi1 = factory.create(jcl, "test.HiveInt");
//        Object hi2 = factory.create(jcl, "test.HiveInt");
//
//        result = method.invoke(obj, hi1, hi2);
//
//        System.out.println(result);
    }

    private static void printUsage() {
        System.out.println("java -jar <container.jar> <jar location> <properties>");
    }

    private static void publishEndPoint(String appName) {
        Injector injector = InjectorSingleton.getInjector();
        ContainerProxy proxy = injector.getInstance(ContainerProxy.class);

        String hostName = properties.getProperty("host", "localhost");
        int port = Integer.parseInt(properties.getProperty("port", "8080"));

        String address = String.format("http://%s:%d/container", hostName, port);
        Endpoint.publish(address, proxy);
        LoggerFactory.getLogger().info(String.format("Container with application %s has started successfully (Address: http://%s:%d/container)", appName, hostName, port));
    }

    private static void loadAppProperties(String propertiesPath) {
        properties = new Properties();

        try {
            //  Load properties file
            properties.load(new FileInputStream(propertiesPath));
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Can't parse application properties. " + e.getMessage());
            System.exit(1);
        }
    }
}
