package org.hive.container.lib;

import com.google.inject.Inject;
import org.apache.log4j.Logger;
import org.hive.container.manifest.HiveMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.JclObjectFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/16/13
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class  ApplicationRegisterImpl implements ApplicationRegister {
    private Map<String, Application> register;

    @Inject
    ApplicationRegisterImpl() {
        register = new HashMap<String, Application>();
    }

    @Override
    public String addApplication(String jarLocation) {
        Application newApp = new Application(jarLocation);
        register.put(newApp.getName(), newApp);

        return newApp.getName();
    }

    @Override
    public String getApplicationInterface(String name, String page) throws SAXException, ParserConfigurationException {
        return register.get(name).getInterface(page, Application.APPLICATION_INTERFACE_XML);
    }

    /**
     * Get an instance of the Application object from registry
     * @param name Name of the application
     * @return An instance of the application
     */
    @Override
    public Application getInstance(String name) {
        return register.get(name);
    }

    @Override
    public String getApplicationTypes(String name) {
        return register.get(name).getTypes();
    }

}
