package org.hive.container.lib;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class JarReader {
    public Document readXML(String jarLocation, String fileToRead) throws ParserConfigurationException, SAXException {
        try {
            ZipFile zipFile = new ZipFile(jarLocation);
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();
                // if the entry is not directory and matches relative file then extract it
                if (!entry.isDirectory() && entry.getName().equals(fileToRead)) {
                    BufferedInputStream bis = new BufferedInputStream(
                            zipFile.getInputStream(entry));

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document manifest = dBuilder.parse(bis);
                    manifest.getDocumentElement().normalize();

                    return manifest;
                } else {
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String readFile(String jarLocation, String fileToRead) {
        System.out.println(fileToRead);

        try {
            ZipFile zipFile = new ZipFile(jarLocation);
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();

                if (!entry.isDirectory() && entry.getName().equals(fileToRead)) {
                    BufferedInputStream bis = new BufferedInputStream(zipFile.getInputStream(entry));

                    return new String(ByteStreams.toByteArray(bis), Charsets.UTF_8);
                } else {
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
