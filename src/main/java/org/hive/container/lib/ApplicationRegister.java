package org.hive.container.lib;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/16/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ApplicationRegister {
    public String addApplication(String jarLocation);

    /**
     * Returns XML (JavaFX-compatible) layout of application
     * @param name Application name (not class name!)
     * @param page Name of the page
     * @return XML layout
     * */
    String getApplicationInterface(String name, String page) throws SAXException, ParserConfigurationException;

    /**
     * Get an instance of the Application object from registry
     * @param name Name of the application
     * @return An instance of the application
     */
    Application getInstance(String name);

    /**
     * Returns XSD description of the return types
     * @param applicationName Application name (not class name!)
     * @return XSD description document
     * */
    String getApplicationTypes(String applicationName);
}
