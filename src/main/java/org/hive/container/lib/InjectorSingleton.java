package org.hive.container.lib;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.hive.container.ContainerModule;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/17/13
 * Time: 9:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class InjectorSingleton {
    private static Injector injector;

    public static Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new ContainerModule());
        }

        return injector;
    }
}
