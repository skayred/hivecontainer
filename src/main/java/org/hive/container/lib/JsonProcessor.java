package org.hive.container.lib;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.util.HashMap;
import java.util.Map;

public class JsonProcessor {

    /**
     * Преобразование Json'а в map
     * @param json Исходный Json
     * @return Map с данными или null, если не удалось разобрать json
     */
    public static Map<String, String> jsonToMap(String json) {
        Map<String, String> map = null;
        ObjectMapper mapper = new ObjectMapper();

        try {
            map = mapper.readValue(json, new TypeReference<HashMap<String, String>>(){});
        } catch (Exception e) {
            LoggerFactory.getLogger().error("Can't build map from json. " + e.getMessage());
        }

        return map;
    }

}
