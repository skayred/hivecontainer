package org.hive.container.proxy;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for client requests to applications
 */
public class Request {

    /**
     * Name of the target class
     */
    private String applicationName;

    /**
     * Name of the target methods
     */
    private String methodName;

    /**
     * List of arguments of the target method
     */
    private List<Object> args;

    public Request() {
        applicationName = null;
        methodName = null;
        args = null;
    }

    /**
     * @param applicationName Name of the target class
     * @param methodName Name of the target methods
     * @param args List of arguments of the target method
     */
    public Request(String applicationName, String methodName, List<Object> args) {
        this.applicationName = applicationName;
        this.methodName = methodName;
        this.args = new ArrayList<Object>(args);
    }

    /**
     * Check request validity
     * @return True if request is valid
     */
    public boolean isValid() {
        return applicationName != null && !applicationName.isEmpty() &&
               methodName != null && !methodName.isEmpty() &&
               args != null;
    }

    /**
     * Get an array of types of arguments of the target method
     * @return An array of types
     */
    public Class[] getArgsTypes() {
        ArrayList<Class> argsTypes = new ArrayList<Class>();

        for (Object arg : args) {
            argsTypes.add(arg.getClass());
        }

        return argsTypes.toArray(new Class[argsTypes.size()]);
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<Object> getArgs() {
        return args;
    }

    public void setArgs(List<Object> args) {
        if (args == null) {
            this.args = new ArrayList<Object>();
            return;
        }
        this.args = new ArrayList<Object>(args);
    }

    public void addArg(Object arg) {
        if (args == null) {
            args = new ArrayList<Object>();
        }

        args.add(arg);
    }

    public void addArgs(List<Object> args) {
        if (this.args == null) {
            this.args = new ArrayList<Object>();
        }

        args.addAll(args);
    }

}
