package org.hive.container.proxy;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.util.List;

/**
 * Interface for communication between any client and specific application from this container
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface ContainerProxy {

    /**
     * Calling specific method from the container
     //     * @param request Description of the target method
     * @param applicationName Name of the application (not class!!!) to call
     * @param methodName Name of the method to invoke
     * @param arguments List of arguments to pass to the method
     * @return Some response of the method
     */
    @WebMethod
    public Object processRequest(String applicationName, String methodName, List<Object> arguments);

    /**
     * Returns the XML (JavaFX-compatible) of one of the application's pages
     * @param applicationName
     * @param page
     * @return
     */
    @WebMethod
    String showInterface(String applicationName, String page);

    /**
     * Returns XSD definition of all application return types
     * */
    @WebMethod
    String getClassesDefinitions(String applicationName);
 }
