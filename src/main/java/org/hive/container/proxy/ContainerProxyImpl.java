package org.hive.container.proxy;

import org.hive.container.lib.Application;
import org.hive.container.lib.ApplicationRegister;
import org.hive.container.lib.LoggerFactory;
import org.hive.container.manifest.HiveMethod;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the ContainerProxy interface functionality
 */
@WebService
public class ContainerProxyImpl implements ContainerProxy {
    @Inject
    private ApplicationRegister register;

    @Inject
    ContainerProxyImpl() {}

    /**
     * Calling specific method from the container
//     * @param request Description of the target method
     * @param applicationName Name of the application (not class!!!) to call
     * @param methodName Name of the method to invoke
     * @param arguments List of arguments to pass to the method
     * @return Some response of the method
     */
    @Override
//    public Object processRequest(Request request) {
    public Object processRequest(String applicationName, String methodName, List<Object> arguments) {
        //  Временна заглушка чтобы не переписывать остальной код
        Request request = new Request(applicationName, methodName, arguments);

        if (request == null || !request.isValid()) {
            //  TODO: Придумать нормальное описание ошибки.
            LoggerFactory.getLogger().fatal("Bad request! \nClass:" + request.getApplicationName() + "\nMethod:" + request.getMethodName() + "\nArgs:" + request.getArgs());
            return  null;
        }

        //  Get an instance of the requested application
        Application application = register.getInstance(request.getApplicationName());
        if (application == null) {
            LoggerFactory.getLogger().fatal(String.format("Can't load class %s.", request.getApplicationName()));
            return null;
        }

        // Invoke target method
        return application.invokeMethod(request.getMethodName(), request.getArgs().toArray());
    }

    /**
     * Returns the XML (JavaFX-compatible) of one of the application's pages
     * @param applicationName
     * @param page
     * @return
     */
    @Override
    public String showInterface(String applicationName, String page) {
        try {
            return register.getApplicationInterface(applicationName, page);
        } catch (SAXException e) {
            LoggerFactory.getLogger().fatal("Failed to parse interface", e);
        } catch (ParserConfigurationException e) {
            LoggerFactory.getLogger().fatal("Failed to initialize parser", e);
        }

        return null;
    }

    @Override
    public String getClassesDefinitions(String applicationName) {
        return register.getApplicationTypes(applicationName);
    }
}
