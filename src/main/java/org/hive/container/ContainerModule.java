package org.hive.container;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.hive.container.lib.ApplicationRegister;
import org.hive.container.lib.ApplicationRegisterImpl;
import org.hive.container.proxy.ContainerProxy;
import org.hive.container.proxy.ContainerProxyImpl;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/16/13
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContainerModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ApplicationRegister.class).to(ApplicationRegisterImpl.class).in(Singleton.class);

        bind(ContainerProxy.class).to(ContainerProxyImpl.class);
    }
}
