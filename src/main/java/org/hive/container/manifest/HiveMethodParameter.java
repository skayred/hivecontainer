package org.hive.container.manifest;

import org.w3c.dom.Element;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class HiveMethodParameter {
    private String type;
    private String name;

    private HiveMethodParameter(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public static HiveMethodParameter createParameter(Element xmlDescription) {
        HiveMethodParameter parameter = new HiveMethodParameter(xmlDescription.getAttribute("name"),
                xmlDescription.getAttribute("type"));

        return parameter;
    }

    public String getType() {
        return type;
    }
}
