package org.hive.container.manifest;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xeustechnologies.jcl.JarClassLoader;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 2:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class HiveMethod {
    private String type;
    private String name;
    private List<HiveMethodParameter> parameters;

    private Map<String, Class> primitiveTypes = new HashMap<String, Class>() {{
        put("byte", Byte.TYPE);
        put("short", Short.TYPE);
        put("int", Integer.TYPE);
        put("long", Long.TYPE);
        put("float", Float.TYPE);
        put("double", Double.TYPE);
        put("boolean", Boolean.TYPE);
        put("char", Character.TYPE);
    }};

    private HiveMethod(String name, String type) {
        this.name = name;
        this.type = type;

        parameters = new ArrayList<HiveMethodParameter>();
    }

    public static HiveMethod createMethod(Element xmlDescription) {
        HiveMethod newMethod = new HiveMethod(xmlDescription.getAttribute("name"),
                xmlDescription.getAttribute("type"));

        NodeList params = xmlDescription.getElementsByTagName("parameter");

        for (int i = 0 ; i < params.getLength() ; i++) {
            newMethod.parameters.add(HiveMethodParameter.createParameter((Element) params.item(i)));
        }

        return newMethod;
    }

    public String getName() {
        return name;
    }

    public Method getReflectionMethod(JarClassLoader jcl, Object obj) throws NoSuchMethodException, ClassNotFoundException {
        Class[] argumentClasses = new Class[parameters.size()];

        for (int i = 0 ; i < argumentClasses.length ; i++) {
            HiveMethodParameter param = parameters.get(i);

            if (primitiveTypes.containsKey(param.getType())) {
                argumentClasses[i] = primitiveTypes.get(param.getType());
            } else {
                try {
                    argumentClasses[i] = jcl.loadClass(param.getType());
                } catch (ClassNotFoundException e) {
                    argumentClasses[i] = Class.forName(param.getType());
                }
            }
        }

        return obj.getClass().getMethod(name, argumentClasses);
    }
}
